Lugus Source Project Management System
======================================

Lugus is designed to quickly start projects with an archetype style.  Within Gitlab is a group called 'archetypes'.
this group contains all the available startup projects in Lugus.

when cloning lugus, take note that it contains a git submodule for the gitignore system.  In addition to the basic

`git clone` command, you must also do:

`git submodule init`

and

`git submodule update`

This will bring down the submodules and you will have a complete working set.