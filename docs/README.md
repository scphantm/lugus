Default Sphinx Documentation
============================

This project contains the default Sphinx documentation for the entire system.  When a new project is initialized, this repo is checked out and the files in here are used as the default to build everything from