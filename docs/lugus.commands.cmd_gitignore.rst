lugus.commands.cmd_gitignore module
===================================


.. automodule:: lugus.commands.cmd_gitignore
    :members: cli, list_ignores, update_ignores, validate_templates
    :show-inheritance:
    :private-members:

.. autoclass:: lugus.commands.cmd_gitignore.GitIgnore
    :members:
    :show-inheritance:
    :private-members:
