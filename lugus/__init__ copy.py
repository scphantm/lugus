
# this url is hard coded into this system.  It should never change
onus_group_archetype = '63'

onus_gitlab_url = 'https://gitlab.highmark.com'
onus_gitlabtest_url = 'https://gitlabtest.highmark.com'
onus_gitlabhealth_url = 'https://gitlabhealth.highmark.com'
onus_gitlabinc_url = 'https://gitlabmedical.highmark.com'

onus_rufus_url = 'https://rufus.highmark.com'
artifactory_base_url = 'https://artifactory.highmark.com/artifactory'

FILE = 'file'
FILES = 'files'
REPLACE_WITH = 'replace_with'
