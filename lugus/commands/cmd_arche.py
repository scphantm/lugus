import click

from syzygy.lib.ConfigSystem import ConfigParameters
from syzygy.lib.StandardTexts import StandardTexts
from syzygy.pipeline.Archetype import Archetype
from syzygy.pipeline.Rufus import RufusClient
from syzygy.syzygy import pass_context


def _new_proj_get_proj_name():
    texts = StandardTexts()
    return texts.texts.arche_enter_name.get_pretty()


@click.group()
@pass_context
def cli(ctx):
    """
    Allows for processing archetypes in gitlab into nearly ready to code product scaffolds
    """


@cli.command('list')
@pass_context
def list_arches(ctx):
    """Lists archetypes currently configured in the system"""
    arche = Archetype()
    arche.list_projects()


@cli.command('new')
@click.option('--run-arche-only', default=False, is_flag=True,
              help='Will bypass other operations and only execute the arche script.  Mainly to debug the scripts')
@click.option('--rufus_hash', prompt='Enter the Rufus hash for the project.  Just hit enter if you dont '
                                     'have one yet', default='')
@click.option('--new_project', prompt=_new_proj_get_proj_name())
@pass_context
def new_arche_project(ctx, run_arche_only, rufus_hash, new_project):
    """Builds a new project based off an archetype"""

    if rufus_hash != "":
        r = RufusClient()
        r.get_component_hash(rufus_hash)

    click.confirm("You entered: '%s'. Is the new project name correct?" % new_project, abort=True)

    cp = ConfigParameters()
    cp.rufus_hash = rufus_hash
    cp.project_name = new_project
    cp.swing["run_arche_only"] = run_arche_only

    arche = Archetype()
    arche.new_project(cp)
