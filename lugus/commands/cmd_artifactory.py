import click
import os
import yaml

from syzygy.commands import ExplicitDumper
from syzygy.lib.ConfigSystem import ConfigParameters
from syzygy.lib.StandardTexts import StandardTexts
from syzygy.pipeline.ArtifactoryMan import ArtifactoryManager
from syzygy.pipeline.GradleMan import GradleManager
from syzygy.syzygy import pass_context


@click.group()
@pass_context
def cli(ctx):
    """
    Commands to run operations against Artifactory
    """


@cli.command('deleteBuild')
@click.option('--artifactory_username', help='Artifactory username')
@click.option('--artifactory_key', help='Artifactory API Key')
@click.option('--build_name', prompt='Enter the build name', help='Artifactory build name')
@click.option('--version', prompt='Enter the version number', help='Artifactory build number/version')
@pass_context
def deleteBuild(ctx, artifactory_username, artifactory_key, build_name, version):
    """
    Deletes a version for a specific build
    """
    cp = ConfigParameters()
    cp.artifactory_username = artifactory_username
    cp.artifactory_key = artifactory_key
    cp.build_name = build_name
    cp.version = version

    manager = ArtifactoryManager(cp)

    manager.delete_build()

    print("Build deleted successfully")
    exit(0)

@cli.command('showBuilds')
@click.option('--artifactory_username', help='Artifactory username')
@click.option('--artifactory_key', help='Artifactory API Key')
@pass_context
def showBuilds(ctx, artifactory_username, artifactory_key):
    """
    Show all builds
    """
    cp = ConfigParameters()
    cp.artifactory_username = artifactory_username
    cp.artifactory_key = artifactory_key

    manager = ArtifactoryManager(cp)

    manager.show_builds()

    exit(0)

@cli.command('showBuildVersions')
@click.option('--artifactory_username', help='Artifactory username')
@click.option('--artifactory_key', help='Artifactory API Key')
@click.option('--build_name', prompt='Enter the build name', help='Artifactory build name')
@pass_context
def showBuilds(ctx, artifactory_username, artifactory_key, build_name):
    """
    Show all builds
    """
    cp = ConfigParameters()
    cp.artifactory_username = artifactory_username
    cp.artifactory_key = artifactory_key
    cp.build_name = build_name

    manager = ArtifactoryManager(cp)

    manager.show_builds_versions()

    exit(0)

@cli.command('showBuildInfo')
@click.option('--artifactory_username', help='Artifactory username')
@click.option('--artifactory_key', help='Artifactory API Key')
@click.option('--build_name', prompt='Enter the build name', help='Artifactory build name')
@click.option('--version', prompt='Enter the version number', help='Artifactory build number/version')
@pass_context
def showBuildInfo(ctx, artifactory_username, artifactory_key, build_name, version):
    """
    Show information for a build
    """
    cp = ConfigParameters()
    cp.artifactory_username = artifactory_username
    cp.artifactory_key = artifactory_key
    cp.build_name = build_name
    cp.version = version

    manager = ArtifactoryManager(cp)

    manager.show_build_info()

    exit(0)