import click
import os
import pkg_resources
import re
from onus.grid.grid_print import GridPrint

from lugus.lugus import pass_context

"""Git Ignore module.

This module is executed by doing a::

    lugus gitignore

command

"""
# noinspection PyUnusedLocal
@click.group()
@pass_context
def cli(ctx):
    """Uses common templates to build and update standard .gitignore files"""
    pass


# noinspection PyUnusedLocal
@cli.command('list')
@pass_context
def list_ignores(ctx):
    """Lists prebuilt gitignore files in the system"""
    ignores = GitIgnore()

    display_list = list(ignores.base_files.keys())

    g = GridPrint()
    print(g.print_grid(sorted(display_list), False))


# noinspection PyUnusedLocal
@cli.command('update')
@pass_context
def update_ignores(ctx):
    """Updates an existing .gitignore file in the current folder"""
    ignores = GitIgnore()
    ignores.update_gitignore(os.getcwd())


# noinspection PyUnusedLocal
def validate_templates(ctx, param, value):
    """Used by Click to validate the user input on the generate command and by the class object on the update

    Args:
        ctx (object): Click context object
        param (object): Click parameter object of some kind
        value (str): The value the user entered into the screen

    Raises:
        Click.BadParameter: Raised if one of the templates entered doesn't exist.

    Returns:
        str: Passes back the original value object

    """
    values = value.split(',')
    ignores = GitIgnore()
    for v in values:
        if v.strip() not in ignores.base_files.keys():
            raise click.BadParameter(v + ' is not a valid template')

    return value


# noinspection PyUnusedLocal
@cli.command('gen')
@pass_context
@click.option('--template', prompt='List the templates you want to use in your .gitignore',
              default='windows,osx,gradle,eclipse,jetbrains',
              callback=validate_templates)
def gen_ignores(ctx, template):
    """Generates a new .gitignore file in the current folder.  Overwriting any existing one"""
    ignores = GitIgnore()
    ignores.gen_gitignore(os.getcwd(), template)


class GitIgnore(object):
    """The primary logic class for the Git Ignore system.  The Git Ignore uses template files from github to
    generate the best .gitignore files for the project.  You can enter the different parameters you wish to
    use within the system, or edit your .gitignore directly and update it.

    The zero parameter constructor initializes the main objects.  It grabs the list of ignore templates from the
    zipped package and loads them into a dictionary.  It then takes the primary file name as the key to the entry
    and also records in it the full path to the object.

    The base_files field ends up looking like this::

        {
            'windows': {'filename': 'Windows.gitignore', 'resource_path': 'gitignore/Global'}
        }

    That is stored away and can be used in later algorithms

    Attributes:
        resource_package (str): Hard coded to 'lugus'
        resource_path (str): The directory in the lugus module that contains the git ignore files.  'gitignore'
        base_files (dict): The generated dictionary that contains all the ignore templates in the system

    """

    def __init__(self):
        # we are going to init the objects that are used in building the system
        self.resource_package = 'lugus'  # Could be any module/package name.
        self.resource_path = 'gitignore'

        tmp_files = pkg_resources.resource_listdir(self.resource_package, self.resource_path)

        self.base_files = {}

        self._build_file_object(tmp_files, self.resource_path)

        resource_path = os.path.join('gitignore', 'Global')
        tmp_files = pkg_resources.resource_listdir(self.resource_package, resource_path)

        self._build_file_object(tmp_files, resource_path)

    def _build_file_object(self, tmp_files, resource_path):
        """This takes the list of template files and uses them to generate the base_files result.  This method
        also filters out anything that's in the resource path that isn't an .gitignore template.

        Args:
            tmp_files (list): A string list of all the template file names
            resource_path (str): Path where the templates were collected

        """
        for fname in tmp_files:
            filename, file_extension = os.path.splitext(fname)

            if file_extension == '.gitignore':
                self.base_files[filename.lower()] = {'filename': fname, 'resource_path': resource_path}

    def gen_gitignore(self, create_path, temps, user_block=None):
        """Assembles all the pieces of the new .gitignore and writes it to the passed path.

        Args:
            create_path (str): The path the ignore file will be created in
            temps (str): comma delimited list of templates to include in the ignore file
            user_block (str): If a file is being updated, the existing files user block is passed here and included
                in the new file
        """
        values = temps.split(',')

        top_block = ['######', '# Generated by lugus',
                     '# As a shortcut, you can add new templates to this list and do ',
                     '# a "lugus gitignore update" to generate an updated file',
                     '# templates:' + temps, '######', '', '']

        if not user_block:
            top_block.extend(['# {user_block',
                              '# It is safe to add user entries in this block.',
                              '# If the file is updated by lugus, these entries will be retained.',
                              '# Any changes made outside this block will be deleted on update', '', '', '',
                              '# user_block}'])
        else:
            top_block.append(user_block)

        top_block.extend(['', ''])

        # just to make the top cleaner
        result = [s + '\n' for s in top_block]

        for val in values:
            res = self.base_files[val]
            path = os.path.join(res['resource_path'], res['filename'])
            data = pkg_resources.resource_string(self.resource_package, path)
            # this comes out of the file as a byte array.  have to convert it to string
            result.append(data.decode("utf-8"))

        ignore_path = os.path.join(create_path, '.gitignore')
        with open(ignore_path, 'w') as file:
            for item in result:
                file.write(item)

    def update_gitignore(self, create_path):
        """Updates an existing ignore file.  You pass in the fully qualified folder that the target ignore file
        is located in.  This method will open the file and extract the templates list and the user block from
        the existing file.  It will then prompt the user for the new template list.  Once the user enters the new
        template list, the method generates a new file, inserting the old user block into it.

        Args:
            create_path (str): Path that the file you want to update is in

        Warning:
            This method will overwrite any user inserted data that is outside of the user block

        """

        ignore_path = os.path.join(create_path, '.gitignore')

        with open(ignore_path, 'r') as my_file:
            data = my_file.read()

        user_block = re.search(r'(# \{user_block)([\s\S]+)(# user_block})', data, re.DOTALL)
        # pprint(user_block.group(0))

        templates = re.search(r'(?<=# templates:)(.+)', data)

        template_input = None
        while True:
            template_input = click.prompt(text="Enter the template string you wish to use for the new file",
                                          default=templates.group(0))
            try:
                validate_templates(None, None, template_input)
            except click.BadParameter as e:
                print(e.message)
            else:
                print('Updating the pattern to ' + template_input)
                break

        self.gen_gitignore(create_path, template_input, user_block.group(0))
