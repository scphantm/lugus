import logging
from logging.handlers import RotatingFileHandler

import click
import sys
from pythonjsonlogger import jsonlogger
from requests.exceptions import HTTPError
from yaml import load

from syzygy.lib import DictAsMember
from syzygy.pipeline.Rufus import RufusClient

try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper

from gitlab import GitlabGetError, OWNER_ACCESS

from gitlab.v4.objects import ProjectBranch, Project

from syzygy.pipeline.Gitlab import GitlabClient
from syzygy.syzygy import pass_context

GITLAB_CONFIG_PROJECT_ID = 5
ANALYSIS_KEY = "syzygy_gitlab_analysis"


@click.group()
@pass_context
def cli(ctx):
    """
    Functions that allow for querying and managing Gitlab.  Some functions, you must be a Gitlab administrator to
    run
    """


@cli.command('list')
@pass_context
def list_projects(ctx):
    """
    Lists all of the available Gitlab Projects.
    :param ctx:
    :return:
    """

    _do_list_projects()


def _do_list_projects():
    gl = GitlabClient()
    try:
        myPage = 1

        projects = gl.gl.projects.list(page=myPage, per_page=10)
        while projects:
            myPage = myPage + 1

            for project in projects:
                msg = """===========================================================
                Project: {}
                Project Desc: {}
                Project Path: {}""".format(project.name, project.description,
                                           project.path_with_namespace)
                print(msg)

            click.confirm("Continue?", abort=True)

            projects = gl.gl.projects.list(page=myPage, per_page=10)
    except GitlabGetError:
        print("Could not get projects")


@cli.command('search')
@click.option("--keyword", prompt="Search string")
@pass_context
def search_projects(ctx, keyword):
    """
    Search Gitlab Projects based on keyword.
    :param ctx, keyword:
    :return:
    """

    _do_search_projects(keyword)


def _do_search_projects(myKeyword):
    gl = GitlabClient()
    try:
        myPage = 1

        projects = gl.gl.projects.list(page=myPage, per_page=10,
                                       search=myKeyword)
        while projects:
            myPage = myPage + 1

            for project in projects:
                msg = """===========================================================
                Project: {}
                Project ID: {}
                Project Desc: {}
                Project Path: {}""".format(project.name, project.id,
                                           project.description,
                                           project.path_with_namespace)
                print(msg)

            click.confirm("Continue?", abort=True)

            projects = gl.gl.projects.list(page=myPage, per_page=10,
                                           search=myKeyword)
    except GitlabGetError:
        print("Could not get projects")


@cli.command('query')
@pass_context
def list_arches(ctx):
    """
    Queries Gitlab for a specific piece of data.
    :param ctx:
    :return:
    """
    promptMsg = """
    
     1) get Project ID
    Enter the query you want to run:
    
    """
    command = click.prompt(click.style(promptMsg, fg='green'), type=int)

    return {
        1: _do_query_projectID(),
    }[command]


def _do_query_projectID():
    path = click.prompt(
        'Enter the full path to the project, for example `hmhs/frameworks/syzygy/syzygy_gradle`')

    gl = GitlabClient()
    try:
        project = gl.get_project(path.strip())

        rufus = RufusClient()
        data = rufus.get_component_by_path(project.path_with_namespace)

        msg = """
        Project ID: {}
        Rufus Hash: {}
        Project Name: {}
        Project Description: {}
        """.format(project.id, data["link_value"], project.name,
                   project.description)

        print(msg)
    except GitlabGetError:
        print("Project path not found")


@cli.command('analyze')
@click.option("--outputfile", prompt="Output File")
@pass_context
def analyze_config(ctx, outputfile):
    configure_logging(outputfile)

    gl = GitlabClient()

    current_user = gl.gl.user
    if not current_user.is_admin:
        logging.critical("You must be a Gitlab Administrator to run analysis")
        sys.exit(1)

    projects = gl.gl.projects.list(as_list=False)

    config_file = _get_check_config_file(gl)

    print("Checking configurations, this may take a few minutes...")
    with click.progressbar(projects) as bar:
        for project in bar:
            if not is_lid(project.namespace["path"]) and not _skip_group(
                project, config_file) and project.visibility != "private":

                _check_group(gl, project, config_file)
                rufus_component_data = _check_rufus(project, config_file)

                # every project has a protected master branch
                _check_branch('master', project, config_file)

                # removing this until we figure out whats going on here
                # if rufus_component_data is not None and rufus_component_data[
                #     "component_deploy_type"] == "RUPGILE_PIPELINE":
                #     _check_branch('tenv2', project, config_file)
                #     _check_branch('tenv6', project, config_file)

                for key in config_file.standard_config.keys():
                    setting = config_file.standard_config[key]
                    _validate_value(setting, key, project, project)


@cli.command('listMerges')
@click.option("--projectid", prompt="Project ID")
@pass_context
def list_merges(ctx, projectid):
    """
    Lists the merge requests for a specific project
    :param ctx, projectid:
    :return:
    """

    _do_list_merges(projectid)


def _do_list_merges(projectId):
    gl = GitlabClient()
    try:
        project = gl.gl.projects.get(projectId)

        merges = project.mergerequests.list()

        for merge in merges:
            msg = """===========================================================
                ID: {}
                Title: {}
                Description: {}
                State: {}""".format(merge.iid, merge.title, merge.description,
                                    merge.state)
            print(msg)
    except GitlabGetError:
        print("Project path not found")


@cli.command('listMergeDetails')
@click.option("--projectid", prompt="Project ID")
@click.option("--mergeid", prompt="Merge Request ID")
@pass_context
def list_merges(ctx, projectid, mergeid):
    """
    Lists the details about a single merge request
    :param ctx, mergeid:
    :return:
    """

    _do_list_merge_details(projectid, mergeid)


def _do_list_merge_details(projectId, mergeid):
    gl = GitlabClient()
    try:
        project = gl.gl.projects.get(projectId)

        merge = project.mergerequests.get(mergeid)

        if merge.author is not None:
            author = merge.author.get("name")
        else:
            author = "None"

        if merge.assignee is not None:
            assignee = merge.assignee.get("name")
        else:
            assignee = "None"

        msg = """
            ID: {}
            Title: {}
            Description: {}
            State: {}
            Created: {}
            Updated: {}
            Target Branch: {}
            Source Branch: {}
            Author: {}
            Assignee: {}""".format(merge.iid, merge.title, merge.description,
                                   merge.state, merge.created_at,
                                   merge.updated_at, merge.target_branch,
                                   merge.source_branch, author, assignee)
        print(msg)
    except GitlabGetError:
        print("Project path not found")


def _skip_group(project, config_file):
    """
    true, skip it.  false, don't skip it
    """
    check_it = False
    for ignore_group in config_file.skip_all_checks.ignore_groups:
        if project.path_with_namespace.startswith(ignore_group):
            print("skipping " + project.path_with_namespace)
            check_it = True
            break
    return check_it


def _check_rufus(project, config_file):
    check_it = True
    for ignore_group in config_file.inventory.ignore_groups:
        if project.path_with_namespace.startswith(ignore_group):
            check_it = False
            break

    if check_it:
        rufus_hash = "rufus_hash"
        rufus = RufusClient()

        try:
            data = rufus.get_component_by_path(project.path_with_namespace)
            return data
        except HTTPError:
            logging.error({ANALYSIS_KEY: {
                **_get_project_header(project),
                'err_key': "rufus_inventory_error",
                'msg': "Project not found in rufus",
                'code': 107,
                'auto_archive': False,
                'path_with_namespace': project.path_with_namespace
            }})

def _check_group(gl, project, config_file):
    group_id = project.namespace['id']
    # print(namespace)
    group = gl.gl.groups.get(group_id)

    check_it = True

    for ignore_group in config_file.owners.ignore_groups:
        if group.full_path.startswith(ignore_group):
            check_it = False
            break

    if check_it:
        for member in group.members.list():
            if member.access_level == OWNER_ACCESS and member.id not in config_file.owners.permitted_owners:
                logging.error({ANALYSIS_KEY: {
                    **_get_project_header(project),
                    'err_key': "owner_error",
                    'msg': "Invalid Owner",
                    'code': 106,
                    'auto_archive': False,
                    'group_full_path': group.full_path,
                    'member_name': member.name,
                    'member_id': member.id
                }})


def _validate_value(setting, key, check_data, project, ):
    """
    This does the actual checking of the value as defined by the config file.
    """
    fail = False

    if _check_if_include_exclude(setting, project):
        if "expected_not_value" in setting:
            if getattr(check_data, key) == setting['expected_not_value']:
                fail = True
        elif "expected_value" in setting:
            if getattr(check_data, key) != setting["expected_value"]:
                fail = True

    if fail:
        if _autofix_if_include_exclude(setting, project):
            fixed = False

            if setting["auto_fix"]:
                _autofix_data_element(check_data, key,
                                      setting['auto_fix_value'])
                fixed = True

            if 'error' in setting:
                logging.error({ANALYSIS_KEY: {
                    **_get_project_header(project),
                    **setting['error'],
                    "fixed": fixed
                }})

            if 'warning' in setting:
                logging.warning({ANALYSIS_KEY: {
                    **_get_project_header(project),
                    **setting['warning'],
                    "fixed": fixed
                }})


def _autofix_if_include_exclude(setting, project):
    includes = []
    excludes = []

    if "auto_fix_excludes" in setting:
        excludes = setting["auto_fix_excludes"]

    if "auto_fix_includes" in setting:
        includes = setting["auto_fix_includes"]

    if project.id in includes:
        # print("project include")
        return True

    if project.id in excludes:
        # print("project exclude")
        return False

    # print("project default")
    return True


def _check_if_include_exclude(setting, project):
    includes = []
    excludes = []

    if "check_excludes" in setting:
        excludes = setting["check_excludes"]

    if "check_includes" in setting:
        includes = setting["check_includes"]

    if project.id in includes:
        # print("project include")
        return True

    if project.id in excludes:
        # print("project exclude")
        return False

    # print("project default")
    return True


def _autofix_branch(check_data, key, to_value):
    gl = GitlabClient()
    config_file = _get_check_config_file(gl)
    # print("autofix branch: " + key + ":" + str(to_value))
    if config_file.protected_branch_config.protected.auto_fix_value:
        can_push = config_file.protected_branch_config.developers_can_push.auto_fix_value
        can_merge = config_file.protected_branch_config.developers_can_merge.auto_fix_value

        check_data.protect(developers_can_push=can_push,
                           developers_can_merge=can_merge)
    else:
        check_data.unprotect()


def _autofix_project(check_data, key, to_value):
    setattr(check_data, key, to_value)
    # try:
    check_data.save()
    # except GitlabUpdateError:
    #     print("Error updating project " + check_data.name)
    #     traceback.print_stack()


autofixDict = {ProjectBranch: _autofix_branch, Project: _autofix_project}


def _autofix_data_element(check_data, key, to_value):
    """
    In the API, different object types have different ways of updating.  This
    method selects on type to ensure that the proper code is used for the object
    """
    autofixDict[type(check_data)](check_data, key, to_value)


def _check_branch(branch_name, project, config_file):
    branch_config = config_file.protected_branch_config

    create_it = False
    check_it = True

    try:
        branch = project.branches.get(branch_name)
    except GitlabGetError:
        logging.warning({ANALYSIS_KEY: {
            "message": "Has no " + branch_name + "branch",
            **_get_project_header(project)}})

        check_it = False

        if branch_name == "master":
            create_it = False  # this indicates that the repo was never initialized in gitlab.
        else:
            create_it = True

    if create_it:
        branch = project.branches.create(
            {'branch': branch_name, 'ref': 'master'})

    if check_it:
        for key in config_file.protected_branch_config.keys():
            setting = config_file.protected_branch_config[key]
            _validate_value(setting, key, branch, project)


def _get_check_config_file(gl):
    blob_id = 0
    gitlabProject = gl.gl.projects.get(GITLAB_CONFIG_PROJECT_ID)
    for d in gitlabProject.repository_tree(path='analysis'):
        if d['name'] == 'gitlab_standard_config.yaml':
            blob_id = d['id']
    if blob_id != 0:
        config_file = gitlabProject.repository_raw_blob(blob_id)
        return DictAsMember(load(config_file, Loader=Loader))
    else:
        print("Error getting analysis file")
        return None


def _get_project_header(project):
    proj = {"project.id": project.id,
            "project.name": project.name,
            "project.description": project.description,
            "project.web_url": project.web_url}

    return proj


def is_lid(possible):
    if len(possible) == 7:
        if possible.startswith('lid'):
            return True
        else:
            return False
    else:
        return False


def configure_logging(outputfile):
    logger = logging.getLogger()
    handler = RotatingFileHandler(outputfile, maxBytes=10 * 1024 * 1024,
                                  backupCount=5)

    # formatter = CustomJsonFormatter('(timestamp) (level) (name) (message)')
    supported_keys = [
        'asctime',
        'created',
        'funcName',
        'levelname',
        'levelno',
        'lineno',
        'module',
        'msecs',
        'message',
        'name',
        'process',
        'processName',
        'relativeCreated',
        'thread',
        'threadName'
    ]

    log_format = lambda x: ['%({0:s})'.format(i) for i in x]
    custom_format = ' '.join(log_format(supported_keys))
    formatter = jsonlogger.JsonFormatter(custom_format)

    handler.setFormatter(formatter)
    logger.addHandler(handler)
    logger.setLevel(logging.INFO)

    logger.addHandler(handler)
