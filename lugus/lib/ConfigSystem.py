import os, sys
from shutil import copyfile
from types import *

from yaml import load

from syzygy.lib import DictAsMember
from syzygy.lib.StandardTexts import StandardTexts

try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper


class ConfigSystem:
    class __ConfigSystem:
        def __str__(self):
            return 'self' + self.val
        def __init__(self):
            """
            This reads in the config file.  If the config file doesnt exist, it copies in a blank one
            and prompts the user to fill it out
            :return:
            """
            self.val = None
            texts = StandardTexts()
            self.usable_texts = texts.texts

            config_file = os.path.expanduser("~/.syzygy.yaml")

            if not os.path.isfile(config_file):
                # file not found.  Copy in a blank one
                copyfile(self.get_blank_config_file_path(), config_file)
                print(self.usable_texts.config_file_not_found.get_pretty())
                sys.exit()

            self.conf = DictAsMember(load(open(config_file), Loader=Loader))
            self.verify_config()
            print(self.usable_texts.config_valid.get_pretty())

        def verify_config(self):
            """
            this method is designed to verify that the configuration file is filled out correctly
            :return:
            """

            # gitlab configs are required to be done because so much of this system requires
            # gitlab api access.
            # lets start with the arche configs, since that is the first system to be brought online
            if self.conf.gitlab.user_name is None:
                self.kill_app(self.usable_texts.config_gitlab_name_not_set.get_pretty())

            if self.conf.gitlab.ssl_private_key is None:
                self.kill_app(self.usable_texts.config_gitlab_ssl_not_set.get_pretty())

            if not os.path.isfile(self.conf.gitlab.ssl_private_key):
                self.kill_app(self.usable_texts.config_gitlab_ssl_not_valid.get_pretty())

            if self.conf.gitlab.private_token is None:
                self.kill_app(self.usable_texts.config_gitlab_token_not_set.get_pretty())

            try:
                if self.conf.gitlab.instance is None:
                    self.kill_app(self.usable_texts.config_gitlab_instance.get_pretty())
            except KeyError:
                self.kill_app(self.usable_texts.config_gitlab_instance.get_pretty())

            if "artifactory" in self.conf:
                if self.conf.artifactory.user_name is None:
                    print(self.usable_texts.config_artifactory_user_not_set.get_pretty())

                if self.conf.artifactory.api_key is None:
                    print(self.usable_texts.config_artifactory_key_not_set.get_pretty())
            else:
                print(self.usable_texts.config_artifactory_key_not_set.get_pretty())

        def get_artifactory_user(self, config_parameters):
            if config_parameters.artifactory_username is not None:
                return config_parameters.artifactory_username

            if "artifactory" not in self.conf:
                self.kill_app(self.usable_texts.config_artifactory_user_not_set_error.get_pretty())

            if self.conf.artifactory.user_name is not None:
                return self.conf.artifactory.user_name

            self.kill_app(self.usable_texts.config_artifactory_user_not_set_error.get_pretty())

        def get_artifactory_key(self, config_parameters):
            if config_parameters.artifactory_key is not None:
                return config_parameters.artifactory_key

            if "artifactory" not in self.conf:
                self.kill_app(self.usable_texts.config_artifactory_key_not_set_error.get_pretty())

            if self.conf.artifactory.api_key is not None:
                return self.conf.artifactory.api_key

            self.kill_app(self.usable_texts.config_artifactory_key_not_set_error.get_pretty())


        def kill_app(self, msg):
            print(msg)
            sys.exit()

        def get_blank_config_file_path(self):
            currdir = os.path.dirname(__file__)
            return os.path.join(currdir, 'syzygy_config.yaml')
    instance = None
    def __new__(cls): # __new__ always a classmethod
        if not ConfigSystem.instance:
            ConfigSystem.instance = ConfigSystem.__ConfigSystem()
        return ConfigSystem.instance
    def __getattr__(self, name):
        return getattr(self.instance, name)
    def __setattr__(self, name):
        return setattr(self.instance, name)



class ConfigParameters:
    def __init__(self):
        self.artifactory_username = None
        self.artifactory_key = None
        self.analysis_file = None
        self.rufus_hash = None
        self.project_name = None
        self.swing = {}
        self.build_name = None
        self.version = None
