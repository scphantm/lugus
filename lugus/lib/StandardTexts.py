import os

from yaml import load

from syzygy.lib.string_formatter import StringFormatter

try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper


class StandardTexts:

    def __init__(self):

        config_file = 'text.yaml'

        currdir = os.path.dirname(__file__)

        config_file = os.path.join(currdir, config_file)

        if config_file is not None:
            self.texts = StringFormatter(load(open(config_file), Loader=Loader))
