import os, sys, click

"""
This file is the main executor of the program.  All validation and option logic is contained here.

This is done so that the various supporting classes can be much cleaner.  Plus with the global scope options
of python, this allows for some more consise code here as well.
"""

CONTEXT_SETTINGS = dict(auto_envvar_prefix='LUGUS')


class Context(object):

    def __init__(self):
        self.verbose = False
        self.home = os.getcwd()

    def log(self, msg, *args):
        """Logs a message to stderr."""
        if args:
            msg %= args
        click.echo(msg, file=sys.stderr)

    def vlog(self, msg, *args):
        """Logs a message to stderr only if verbose is enabled."""
        if self.verbose:
            self.log(msg, *args)


pass_context = click.make_pass_decorator(Context, ensure=True)
cmd_folder = os.path.abspath(os.path.join(os.path.dirname(__file__), 'commands'))


class ComplexCLI(click.MultiCommand):
    """
    Complex CLI Class allows the plugin system to work.  It provides functions that list possible commands
    in the command folder and retrieves them when passed in by the cli engine
    """
    def list_commands(self, ctx):
        rv = []
        for filename in os.listdir(cmd_folder):
            if filename.endswith('.py') and \
               filename.startswith('cmd_'):
                rv.append(filename[4:-3])
        rv.sort()
        return rv

    def get_command(self, ctx, name):
        try:
            if sys.version_info[0] == 2:
                name = name.encode('ascii', 'replace')
            mod = __import__('lugus.commands.cmd_' + name, None, None, ['cli'])
        except ImportError as err:
            print("Import Error: {0}".format(err))
            return
        return mod.cli


@click.command(cls=ComplexCLI, context_settings=CONTEXT_SETTINGS)
@pass_context
def cli(ctx):
    """
    Lugus code management utility.

    Hello boss, lets build some software
    """