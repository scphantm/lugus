from yaml import load
import os

# constants that are mapped to functions within the code
PROJECT_VERSION = 'project_version'
PROJECT_LANGUAGES = 'project_languages'
PROJECT_GROUP = 'project_group'
PROJECT_NAME = 'project_name'

try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper


class MetadataNotFound(Exception):
    def __init__(self, path_tried):
        Exception.__init__(self, "Failed to load Metadata file.  Tried path: " + path_tried)


class MetadataParser(object):
    def __init__(self, path):
        """
        In this method we are going to load the metadata.yaml file that should be in the project root.  The lang specific
        parsers can extend this class to do whatever they want
        """

        self.metadata = None

        if not os.path.isfile(path):
            raise MetadataNotFound(path)
        else:
            self.metadata = load(open(path), Loader=Loader)

        self.project_version = self.metadata[PROJECT_VERSION]
        self.project_languages = self.metadata[PROJECT_LANGUAGES]
        self.project_group = self.metadata[PROJECT_GROUP]
        self.project_name = self.metadata[PROJECT_NAME]


def get_project_version(metadata_path):
    m = MetadataParser(metadata_path)
    if m.project_version is not None:
        return m.project_version
    else:
        return ''


def get_project_languages(metadata_path):
    m = MetadataParser(metadata_path)
    if m.project_languages is not None:
        return m.project_languages
    else:
        return []


def get_project_group(metadata_path):
    m = MetadataParser(metadata_path)
    if m.project_group is not None:
        return m.project_group
    else:
        return ''


def get_project_name(metadata_path):
    m = MetadataParser(metadata_path)
    if m.project_name is not None:
        return m.project_name
    else:
        return ''