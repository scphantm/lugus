import os
import shutil
from pprint import pprint

from syzygy.lib.ConfigSystem import ConfigSystem
from syzygy.lib.StandardTexts import StandardTexts
from syzygy.lib.grid_print import GridPrint
from syzygy.lib.string_formatter import StringFormatter
from syzygy.pipeline.GitMan import clone_archetype_project
from syzygy.pipeline.Gitlab import GitlabClient
from syzygy.lib.ArcheScript import ArcheScript


class Archetype(object):
    def __init__(self):
        self.config = ConfigSystem()
        self.gitlab = GitlabClient()

        texts = StandardTexts()
        self.usable_texts = texts.texts

    def list_projects(self):
        print(self.usable_texts.arche_arches_list.get_pretty())
        groups = self.gitlab.get_archetype_group()
        self._print_projects_grid(groups)

    def _print_projects_grid(self, groups, include_numbers=True):
        groups_to_print = []
        projects = groups.projects.list()

        for p in projects:
            groups_to_print.append(p.name)

        g = GridPrint()
        g.set_sort(False)
        print(g.print_grid(groups_to_print, include_numbers))

    def new_project(self, config_parameters):
        # get the name of the new project

        run_arche_only = config_parameters.swing["run_arche_only"]

        if not run_arche_only:  # check the script only option
            groups = self.gitlab.get_archetype_group()
            self._print_projects_grid(groups)

            # get the archetype object that the user wants to use as a base
            project_to_use = self._new_proj_get_arche_to_use(groups)

            if project_to_use.name == "springboot":
                sType = "springboot"
            if project_to_use.name == "shell_project":
                sType = "shell"
            if project_to_use.name == "database_project":
                sType = "db"
            if project_to_use.name == "liberty_app":
                sType = "was"
            if project_to_use.name == "javascript_project":
                sType = "js"
            if project_to_use.name == "jvm_library":
                sType = "jvm"

            # clone down the archetype using the new name
            clone_archetype_project(project_to_use, config_parameters.project_name)

        # run archetype script that came down, if it exists
        self._new_proj_run_archetype_script(config_parameters, sType)

        if not run_arche_only:
            # delete archetype folder
            shutil.rmtree(os.path.join(config_parameters.project_name, "archetype"))

    def _new_proj_run_archetype_script(self, config_parameters, sType):
        arch_script_file = os.path.join(config_parameters.project_name, "archetype",
                                        "archetype.py")
        print("Running any archetype scripts within the project.")

        """
        The archetype.py script will execute within the scope of this method.  So we will make life easier
        by initializing some global variables for script writers here.
        """
        arche_script = ArcheScript(config_parameters, self.gitlab.get_groups())

        if os.path.exists(arch_script_file):
            with open(arch_script_file) as f:
                code = compile(f.read(), arch_script_file, 'exec')
                exec(code)

        # now that we have execd the code, we have loaded variables that we can now execute against
        arche_script.run_all_scripts(sType,config_parameters.project_name)

    def _new_proj_get_arche_to_use(self, groups):
        """
        This prompts the user with a list of archetypes to choose from.  The
        user enters a selection and the system
        validates the selection and moves on to returning the object.

        This returns the gitlab project object for the selected archetype.  Not
        just the name, but the whole object
        :param groups:
        :return: the object the user selected
        """
        project_to_use = []
        while True:
            try:
                new_proj_input = int(
                    input(
                        self.usable_texts.arche_pick_project.get_pretty()))
                # get the gitlab project object for the selected archetype
                # pprint(groups.projects)
                project_to_use = groups.projects.list()[new_proj_input - 1]
            except ValueError:
                print()
                continue
            except NameError:
                print(self.usable_texts.arche_invalid_selection.get_pretty())
            else:
                print(project_to_use.name + " selected.")
                break

        return project_to_use

    def _format_line(self, index, name):
        ret_text = " " + StringFormatter.LIGHT_GREEN + str(
            index) + StringFormatter.ENDC
        ret_text += "    "
        ret_text += StringFormatter.LIGHT_PURPLE + name + StringFormatter.ENDC
        return ret_text
