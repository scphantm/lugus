import json
import re

import requests
import urllib3
import untangle

from syzygy import artifactory_base_url
from syzygy.lib import get_first
from syzygy.lib.ConfigSystem import ConfigSystem
from syzygy.lib.HMHSutilities import HMHSUtilities
from syzygy.pipeline.GradleMan import GradleManager
from pprint import pprint
from xml.etree import ElementTree


class ArtifactoryManager:
    """
    HMHS Specific Interface for Artifactory Operations
    """

    # Default constructor
    def __init__(self, config_parameters):
        self.config_parameters = config_parameters
        self.conf = ConfigSystem()

    # Search artifactory by the artifact name
    def artifact_search(self, artifact_name):
        """Search Artifactory by Artifact Name"""
        urllib3.disable_warnings()
        url = artifactory_base_url + "/api/search/artifact?name=" + artifact_name
        response = requests.get(url, auth=(
            self.config_parameters.artifactory_username, self.config_parameters.artifactory_key), verify=False)
        return response.json()

    # Search Artifactory by the GAVC
    def gavc_search(self, artifact_name, group, version):
        """Search Artifactory by GAVC"""
        url = artifactory_base_url + "/api/search/gavc?a=" + artifact_name + "&g=" + group + "&v=" + version

        artifactory_username = self.conf.get_artifactory_user(self.config_parameters)
        artifactory_key = self.conf.get_artifactory_key(self.config_parameters)

        headers = {'X-Result-Detail': 'info, properties'}
        response = requests.get(url, auth=(artifactory_username, artifactory_key), verify=False, headers=headers)

        if("errors" in response.json()):
            print("Error in obtaining Artifactory Information")
            print(response.json()['errors'])
            exit()
        else:
            return response.json()['results']

    def get_dependencies(self):
        gm = GradleManager(self.config_parameters)

        # Read the file into JSON
        json_input = gm.get_analysis()
        error_count = 0

        projects = HMHSUtilities.find_values_in_json(json_input, 'projects')

        projectNames = []
        for proj in projects:
            projectNames.append(proj.get('name'))

        # Look for 'dependencies' in the JSON as a key and return all matches in a list
        depends = HMHSUtilities.find_values_in_json(json_input, 'dependencies')

        # Create a set for unique items
        myset = {}

        # make the list unique
        for item in depends:
            if(item['artifactId'] not in projectNames):
                gavc = item['groupId'] + ":" + item['artifactId'] + ":" +  item['version']
                myset[gavc] = item

        return myset

    def get_dependencies_resolved(self):

        dependency_list = self.get_dependencies()

        # Loop through the list
        for key, item in dependency_list.items():
            # JSON wants " not '
            # item = json.loads(items.replace("'", "\""))

            # Search Artifactory by the GAVC for each dependent item
            item['resolved'] = self.gavc_search(artifact_name=item['artifactId'],
                                         group=item['groupId'],
                                         version=item['version'])

        return dependency_list

    def show_builds(self):
        """Shows all builds in Artifactory"""
        url = artifactory_base_url + "/api/build"

        artifactory_username = self.conf.get_artifactory_user(self.config_parameters)
        artifactory_key = self.conf.get_artifactory_key(self.config_parameters)

        headers = {'X-Result-Detail': 'info, properties'}
        response = requests.get(url, auth=(artifactory_username, artifactory_key), verify=False, headers=headers)

        if (response.status_code != 200):
            print("Error in searching builds in Artifactory")
            print(response.json()['errors'])
            exit()
        else:
            for build in response.json()['builds']:
                print(build['uri'].replace('/',''))

    def show_builds_versions(self):
        """Shows all versions for a build in Artifactory"""
        build_name = self.config_parameters.build_name
        url = artifactory_base_url + "/api/build/" + build_name

        artifactory_username = self.conf.get_artifactory_user(self.config_parameters)
        artifactory_key = self.conf.get_artifactory_key(self.config_parameters)

        headers = {'X-Result-Detail': 'info, properties'}
        response = requests.get(url, auth=(artifactory_username, artifactory_key), verify=False, headers=headers)

        if (response.status_code != 200):
            print("Error in getting build versions in Artifactory")
            print(response.json()['errors'])
            exit()
        else:
            for build in response.json()['buildsNumbers']:
                print("Version: " + build['uri'].replace('/','') + " Date/Time: " + build['started'])

    def show_build_info(self):
        """Shows all versions for a build in Artifactory"""
        build_name = self.config_parameters.build_name
        version = self.config_parameters.version
        url = artifactory_base_url + "/api/build/" + build_name + "/" + version

        artifactory_username = self.conf.get_artifactory_user(self.config_parameters)
        artifactory_key = self.conf.get_artifactory_key(self.config_parameters)

        headers = {'X-Result-Detail': 'info, properties'}
        response = requests.get(url, auth=(artifactory_username, artifactory_key), verify=False, headers=headers)

        if (response.status_code != 200):
            print("Error in getting build info in Artifactory")
            print(response.json()['errors'])
            exit()
        else:
            print(json.dumps(response.json()['buildInfo'], indent=4, sort_keys=True))

    def delete_build(self):
        """Delete a specific build from Artifactory"""
        build_name = self.config_parameters.build_name
        version = self.config_parameters.version
        url = artifactory_base_url + "/api/build/" + build_name + "?buildNumbers=" + version + "&artifacts=1"

        artifactory_username = self.conf.get_artifactory_user(self.config_parameters)
        artifactory_key = self.conf.get_artifactory_key(self.config_parameters)

        headers = {'X-Result-Detail': 'info, properties'}
        response = requests.delete(url, auth=(artifactory_username, artifactory_key), verify=False, headers=headers)


        if(response.status_code != 200):
            print("Error in deleting build in Artifactory")
            print(response.json()['errors'])
            exit()
        else:
            return response

    def check_no_dependency_lower_tier(self):
        dependency_list = self.get_dependencies_resolved()
        error_flag = False

        for key, items in dependency_list.items():

            # If we didn't find a result, mark it as an error
            if len(items['resolved']) < 1:
                print(key + " - Library is not found in Artifactory")
                error_flag = 1
            else:
                tmp_flag = False
                for item in items['resolved']:
                    tmp_flag = {
                        'qa': self._check_ready_for_qa(key, item),
                        'uat': self._check_ready_for_prod(key, item),
                        'prod': self._check_ready_for_prod(key, item),
                    }[self.config_parameters.swing["to_environment"]]
                    if tmp_flag:
                        print(key + " - Library is in a lower repository than " +
                              self.config_parameters.swing["to_environment"] + ", failing")
                        error_flag = True
                        break

        return error_flag, dependency_list

    def _check_ready_for_qa(self, gavc, item):
        if "snapshot" in item['repo']:
            return True

    def _check_ready_for_prod(self, gavc, item):
        if self._check_ready_for_qa(gavc, item):
            return True

        if "qa" in item['repo']:
            return True

    def get_latest_distributions(self):
        data = self._get_dist_folder("gradle")

        versionFolders = self.get_children_details(data, True)
        versionFolders.sort(reverse=True)

        latestVersionFolder = get_first(versionFolders)

        data = self._get_dist_folder("gradle/" + latestVersionFolder)
        childrenList = data['children']
        gradleDists = self.get_children_details(data, False)
        gradleDists.sort()

        reobj = re.compile(r"(?<=.gradle-)(.*)(?=-bin\.zip)", re.MULTILINE)

        versionGradle = []

        for subject in gradleDists:
            match = reobj.search(subject)
            if match:
                versionGradle.append(match.group())

        return latestVersionFolder, versionGradle

    def get_latest_syzygy_version(self, type):

        XML_PATH = "syzygy_jvm/syzygy_jvm"

        if type == "jvm":
            XML_PATH = "syzygy_jvm/syzygy_jvm"
        if type == "db":
            XML_PATH = "syzygy_extended/syzygy-database"
        if type == "js":
            XML_PATH = "syzygy_javascript/syzygy_javascript"
        if type == "shell":
            XML_PATH = "syzygy_extended/syzygy-shell"
        if type == "conf":
            XML_PATH = "syzygy_extended/syzygy-configuration"
        if type == "was":
            XML_PATH = "syzygy_websphere/syzygy-was-liberty"
        if type == "core":
            XML_PATH = "syzygy_gradle/syzygy-core"

        XML = "https://artifactory.highmark.com/artifactory/gradle-plugins/com/hmhs/frameworks/syzygy/" + XML_PATH + "/maven-metadata.xml"

        webresponse = requests.get(XML, verify=False)

        response = untangle.parse(webresponse.text)

        versioning = response.metadata.versioning.latest.cdata

        return versioning


    def get_children_details(self, data, folder_val):
        childrenList = data['children']
        return [x["uri"].replace("/", "") for x in childrenList if x["folder"] == folder_val]

    def _get_dist_folder(self, folder):
        url = artifactory_base_url + "/api/storage/dist/" + folder

        response = requests.get(url, verify=False)
        return response.json()



# Module which would be deployed should not be included in the search.
# TODO: Read this from the command line or configuration file
# appsubapp = 'mqrspm'

# Exit with the number of errors encountered
#
