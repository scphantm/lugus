import gc
import os

from git import Repo

from syzygy.lib import rmtree


def clone_archetype_project(project_to_use, new_proj_name):
    repo = Repo.clone_from(project_to_use.ssh_url_to_repo, new_proj_name)
    gc.collect()
    repo.git.clear_cache()
    rmtree(os.path.join(new_proj_name, ".git"))

