import gitlab

from syzygy import onus_gitlab_url, onus_group_archetype, onus_gitlabtest_url, onus_gitlabhealth_url, onus_gitlabinc_url
from syzygy.lib.ConfigSystem import ConfigSystem


class GitlabClient:
    def __init__(self):
        self.gl = None
        self.config = ConfigSystem()

        self._init_gitlab_object()

    def _init_gitlab_object(self):
        # private token authentication
        
        url = {
            "gitlab": onus_gitlab_url,
            "gitlabtest": onus_gitlabtest_url,
            "gitlabhealth": onus_gitlabhealth_url,
            "gitlabinc": onus_gitlabinc_url
        }.get(self.config.conf.gitlab.instance)

        self.gl = gitlab.Gitlab(url,
                                self.config.conf.gitlab.private_token,
                                api_version='4', ssl_verify=False)
        self.gl.auth()

    def get_archetype_group(self):
        return self.gl.groups.get(onus_group_archetype)

    def get_groups(self):
        return self.gl.groups

    def get_project(self, project_path):
        return self.gl.projects.get(project_path)