import os, sys, json
from subprocess import Popen
from subprocess import check_output
from pathlib import Path


class GradleManager:
    def __init__(self, config_parameters, working_folder=None):
        if not working_folder:
            self.working_folder = os.path.join(os.getcwd(), config_parameters.project_name)
        else:
            self.working_folder = working_folder

        self.build_dir = os.path.join(self.working_folder, "build")
        self.config_parameters = config_parameters

    def run_gradle_tasks(self, gradle_tasks):
        # pprint(self.gradle_tasks)
        if len(gradle_tasks) > 0:
            gradle_cmd = self.get_gradle_exe()
            gradle_cmd.extend(gradle_tasks)

            p = Popen(gradle_cmd, cwd=self.working_folder).wait()

    def run_gradle_tasks_with_output(self, gradle_tasks):
        # pprint(self.gradle_tasks)
        if len(gradle_tasks) > 0:
            gradle_cmd = self.get_gradle_exe()
            gradle_cmd.extend(gradle_tasks)

            p = check_output(gradle_cmd, cwd=self.working_folder)
            return p

    def get_gradle_exe(self):
        gradle_cmd = []
        if os.name == 'nt':
            gradle_cmd = [os.path.join(self.working_folder, "gradlew.bat")]
        else:
            cmd = os.path.join(self.working_folder, "gradlew")
            os.chmod(cmd, 0o755)
            gradle_cmd = [cmd]

        my_file = Path(gradle_cmd[0])
        if not my_file.is_file():
            print("You tried a command that requires Gradle and Gradle was not found")
            print(my_file)
            sys.exit()

        return gradle_cmd

    def get_analysis(self, always_refresh=False):

        if self.config_parameters.analysis_file is None:
            self.get_gradle_exe()
            analysis_file = Path(os.path.join(self.build_dir, "analysis.json"))
        else:
            analysis_file = Path(self.config_parameters.analysis_file)

        gmTasks = self.run_gradle_tasks_with_output(['tasks'])

        if b'print-info' not in gmTasks:
            print('This does not appear to be a Syzygy project.')
            print('This function must be executed from the root of a Syzygy project')
            return None

        if not analysis_file.is_file():
            self.run_gradle_tasks(['print-info'])
        elif always_refresh and self.config_parameters.analysis_file is None:
            self.run_gradle_tasks(['print-info'])

        if not analysis_file.is_file():
            print("Unable to find or create analysis.json file")
            return None

        with open(analysis_file, 'r') as f:
            return json.load(f)

    def get_promote_report_filename(self):
        if self.config_parameters.analysis_file is None:
            self.get_gradle_exe()
            if not  os.path.exists(self.build_dir):
                os.makedirs(self.build_dir)

            use_dir = self.build_dir
        else:
            use_dir = Path(self.config_parameters.analysis_file).parent

        return Path(os.path.join(use_dir, "promote_report.yaml"))


