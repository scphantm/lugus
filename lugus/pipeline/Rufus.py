import requests

from syzygy import onus_rufus_url
from pprint import pprint

class RufusClient:

    def get_component_hash(self, hash, children = False):
        url = onus_rufus_url + "/inventory/application/{}/part"
        url = url.format(hash)

        params = {'include_children': children}
        data = requests.get(url, params=params, headers=self._get_requests_headers(), verify=False)

        json = data.json()

        if json['entityType'] != 'COMPONENT':
            raise HashNotComponent("Entered hash is not a component")

        return json

    def get_component_by_path(self, path):
        url = onus_rufus_url + "/inventory/application/path"
        params = {'path': path}

        data = requests.get(url, params=params, headers=self._get_requests_headers(), verify=False)
        data.raise_for_status()

        json = data.json()

        if json['entityType'] != 'COMPONENT':
            raise HashNotComponent("Entered hash is not a component")

        return json

    def _get_requests_headers(self):
        return {'Accept': 'application/json',
                'User-Agent': 'Mozilla/5.0',
                'Content-Type': 'application/json',
                'HighWireUserName': 'syzygy_python',
                'HighWireUserUID': 'syzygy_python'
                }

class HashNotComponent(Exception):
    def __init__(self, value):
        self.parameter = value
    def __str__(self):
        return repr(self.parameter)