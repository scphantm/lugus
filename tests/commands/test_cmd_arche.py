import unittest
import os

from commands.cmd_arche import ArcheScript


class TestArcheScriptCommands(unittest.TestCase):
    def setUp(self):
        new_proj_name = 'test'

        project_groups = {}
        self.script = ArcheScript(new_proj_name, project_groups)
        self.script.proj_dir = os.path.join(os.getcwd(), "../", "resources", "searchreplace")

    def test_search_replace_keys_happy(self):
        self.script._search_replace_keys()

if __name__ == '__main__':
    unittest.main()