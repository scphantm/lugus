arche_script.directories_to_create = ['src/main/scala', 'src/main/resources']
arche_script.sphinx_initialize = True
arche_script.sphinx_target_folder = 'src/main/sphinx'
arche_script.sphinx_scaffold = 'opus/default_sphinx'